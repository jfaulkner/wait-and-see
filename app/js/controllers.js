'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
    .controller('LandingPageController', [function() {

    }])
    .controller('WaitlistController', ['$scope', '$firebase', function($scope, $firebase) {
        // Connect $scope.parties to live Firebase data.
        var partiesRef = new Firebase('https://waitandeat-jerome.firebaseio.com/parties');

        // Object to store data from the waitlist form.
        $scope.parties = $firebase(partiesRef);

        $scope.newParty = {
            name: '',
            phone: '',
            size: '',
            done: false,
            notified: 'No'
        };

        // Function to save a new party to the waitlist.
        $scope.saveParty = function() {
            $scope.parties.$add($scope.newParty);
            $scope.newParty = {
                name: '',
                phone: '',
                size: '',
                done: false,
                notified: 'No'
            };
        };

        // Function to send a text message to a party.
        $scope.sendTextMessage = function(party) {
            var textMessageRef = new Firebase('http://waitandeat-jerome.firebaseio.com/textMessages');
            var textMessages = $firebase(textMessageRef);
            var newTextMessage = {
                phoneNumber: party.phone,
                size: party.size,
                name: party.name
            };
            textMessages.$add(newTextMessage);

            party.notified = 'Yes';
            $scope.parties.$save(party.$id);
        };
    }]);